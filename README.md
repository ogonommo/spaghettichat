# spaghettichat (simple chat app)

# Team Spaghetti
## Members

* Stoyan Peshev @ [ogonommo](https://my.telerikacademy.com/Users/ogonommo)
* Nikolay Penev @ [NikoPenev](https://my.telerikacademy.com/Users/NikoPenev)
* Georgi Levov @ [GeorgeLevov](https://my.telerikacademy.com/Users/GeorgeLevov)

## App Description

### spaghettichat is a simple chat app, utilizing socket.io and Express for backend, socket.io for frontend, with additional functionality provided by Giphy API and several in-house developed plugins and tools to create a pleasant user experience

### Tools used:

* Express.js
* socket.io
* jQuery
* Materialize
* Giphy APi

### GitLab URL: [spaghettichat](https://gitlab.com/ogonommo/spaghettichat)

### Trello URL: [trello](https://trello.com/b/ewYAChZi/spaghettichat-workflow)