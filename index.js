const express = require('express');
const app = express();
const path = require('path');
const server = require('http').createServer(app);
const io = require('socket.io')(server);
const port = process.env.PORT || 5000;

// init the server
server.listen(port, function() {
  console.log('Server running on port ' + port);
});

// expose main route
app.use(express.static(path.join(__dirname, 'public')));

// global chat variables
const globals = {
  userCount: 0,
  users: [],
  pid: 0,
  polls: [],
  last20messages: [],
};

io.on('connection', (socket) => {
  // if the user has been added and counted in the total
  let userCounted = false;
  let user = null;

  // adding and counting the user
  socket.on('new user', (data) => {
    if (userCounted) {
      return;
    }

    // update the user count and notify the client
    globals.userCount++;
    userCounted = true;

    // add the user to the list of online users
    user = {
      username: data.username,
      chatcolor: data.chatcolor,
      id: globals.pid++,
    };
    globals.users.push(user);

    // send the current users status to the client
    socket.emit('login', {
      userCount: globals.userCount,
      users: globals.users,
      messages: globals.last20messages,
    });
    // send the current users status to all users
    socket.broadcast.emit('user joined', {
      username: data.username,
      users: globals.users,
      userCount: globals.userCount,
    });
  });

  // receiving message from the client app
  socket.on('new message', (msg) => {
    // update the last 20 message list
    if (globals.last20messages.length === 20) {
      globals.last20messages = [...globals.last20messages.slice(1), msg];
    } else {
      globals.last20messages.push(msg);
    }
    // sending the message back to the client app
    // socket.emit('new message', msg);
    socket.broadcast.emit('new message', msg);
  });

  // notify the client an user is typing in the chat
  // not implemented on the client
  socket.on('typing', () => {
    socket.broadcast.emit('typing', {
      username: user.username,
    });
  });

  // notify the client an user has stopped typing in the chat
  // not implemented on the client
  socket.on('stopped typing', () => {
    socket.broadcast.emit('stopped typing', {
      username: user.username,
    });
  });

  // disconnect the user and notify the client
  socket.on('disconnect', () => {
    if (userCounted) {
      globals.userCount--;

      // remove the user from the list of online users
      globals.users = globals.users.filter((u) => u !== user);

      socket.broadcast.emit('disconnected', {
        users: globals.users,
        username: user.username,
        userCount: globals.userCount,
      });
    }
  });
});
