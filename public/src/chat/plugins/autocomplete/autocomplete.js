import {
  fragments,
} from './fragments';
import $ from 'jquery';

// $ exist as a standard <script> import in the html file
// this is a temporary fix

// global vars
let autocompleteContainer = '';
const resultsArray = [];
let resultCursor = 0;

// focus input on login
const autocompleteFocus = (target) => {
  $(target).focus();
};

// check if autocomplete is active
const isActive = () => {
  if ($('.autocomplete-container').length) {
    return true;
  }

  return false;
};

// each click on any autocomplete result, appends it's coomand name to the input
const eachResultClick = (target) => {
  $(document.body).on('click', 'div.result', function(e) {
    const $this = $(this);
    $('.result').removeClass('highlighted');
    $this.addClass('highlighted');
    const commandText = $this.children()[0].innerHTML.trim();
    $(target).val(commandText);
    $(target).focus();
  });
};

// when typing in input, on each press
const eachButtonPress = (target) => {
  $(document.body).on('keyup', '#messageinput', function(event) {
    $('.autocomplete-container').remove();
    let matchingCommands = [];

    // check if there is anything in input
    if ($(target).val().length > 0) {
      matchingCommands = getMatches($(target).val());
    }
    // display the field with commands mayching the input
    if (matchingCommands.length) {
      autocompleteDisplay(target, matchingCommands);

      // vars for highlighting functionality
      const resultsLength = $('.autocomplete-container').children().length;
      let currentResult = $('.autocomplete-container').children()[resultCursor];
      $(currentResult).addClass('highlighted');

      // special key events
      switch (event.keyCode) {
        // enter key
        case 13:
        // replacing the input with the highlighted result's command name
          if ($('.autocomplete-container').length) {
            const newInputValue = $(currentResult).children()[0].innerHTML.trim();
            $(target).val(newInputValue);
            $('.autocomplete-container').remove();
            resultCursor = 0;
          }
          break;
        // arrow up
        case 38:
        // moving up to the previous result
          if (resultCursor > 0) {
            resultCursor -= 1;
            currentResult = $('.autocomplete-container').children()[resultCursor];

            $('.result').removeClass('highlighted');
            $(currentResult).addClass('highlighted');
          }

          break;
        // arrow down
        case 40:
        // moving down to the next result
          if (resultCursor < resultsLength - 1) {
            resultCursor += 1;
            currentResult = $('.autocomplete-container').children()[resultCursor];

            $('.result').removeClass('highlighted');
            $(currentResult).addClass('highlighted');
          }
          break;
        // backspace
        case 8:
        // keeping first result highlighted
          resultCursor = 0;
          break;

        default:
          break;
      }
    }
  });
};

// returns the commands matching the input
const getMatches = (targetInput) => {
  const matchedCommandsArray = [];
  const input = targetInput.toLowerCase();
  fragments.forEach((item) => {
    if (item.command.toLowerCase().includes(input)) {
      matchedCommandsArray.push(item);
    }
  });
  return matchedCommandsArray;
};

// displays the commands matching the input in a container
const autocompleteDisplay = (target, commands) => {
  let resultsContainer = '';

  commands.forEach((item) => {
    const result = `
    <div class="result">
      <div class="result-name"> ${item.command}  </div>
      <div class="result-desc"> ${item.description} </div>
     </div>`;

    resultsContainer += result;
    resultsArray.push(result);

    autocompleteContainer = `
  <div class="autocomplete-container">
  ${resultsContainer}
  </div>`;

    $('.autocomplete-container').remove();
    $(autocompleteContainer).insertBefore($(target));
  });
};


const init = (target) => {
  autocompleteFocus(target);
  eachButtonPress(target);
  eachResultClick(target);
};

export default {
  init,
  isActive,
};
