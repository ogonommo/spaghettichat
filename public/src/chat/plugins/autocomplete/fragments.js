export const fragments = [{
  command: '/giphy',
  description: `/giphy text-for-gif-search<br/>
  insert a gif from Giphy
  use "Shuffle" to cycle through available gifs
  use "Send" to send the selected gif
  use "Cancel" to cancel the command`,
},
{
  command: '/clear',
  description: `/clear
  clears the chat window`,
},
];
