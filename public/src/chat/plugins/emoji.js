import emojiMap from '../parser/microcodes';
import $ from 'jquery';

// appends the button and emoji container to target
const addEmojiButton = (target) => {
  const emoButton = `
  <div class="waves-effect btn-flat white" id="emoji-button">
  <i class="material-icons medium" id="emoji-button-icon">insert_emoticon</i>
  </div>`;

  const emojiTable = `
    <div id="emoji-table" class="white scale-transition scale-out">
    </div> `;

  $(target).append(emoButton);
  $(target).append(emojiTable);

  // when clicked 'escape', hides the emoji container
  $('body').on('keyup', function(e) {
    if (e.which === 27) {
      $('#emoji-table').removeClass('scale-in').addClass('scale-out');
      $('#emoji-table').empty();
    }
  });
};

// on click toggles the emoji container
const showEmojiList = (target) => {
  $(target).on('click', 'div#emoji-button', function() {
    toggleScale($('#emoji-table'));
  });
};

// every selected emoji is appended to the input
const appendEmojiToInput = (target) => {
  $('div.emoji').on('click', function() {
    const emojiText = $(this).text().trim();
    $('input#messageinput').val($('input#messageinput').val() + emojiText);
    $('input#messageinput').focus();
  });
};

// toggle (show/hide) emoji container
const toggleScale = (target) => {
  if ($(target).hasClass('scale-out')) {
    $(target).removeClass('scale-out').addClass('scale-in');
    // Loading emoji elements only on scale-in
    $('#emoji-table').append(loadEmojiTable());
    appendEmojiToInput(target);
  } else {
    $(target).removeClass('scale-in').addClass('scale-out');
    $('#emoji-table').empty();
  }
};

// retuns array with emoji elements loaded '../parser/microcodes'
const loadEmojiTable = () => {
  const emojiArray = [];
  emojiMap.forEach((value, key) => {
    const eachValue = `
      <div class="col s2 emoji" style="padding: 5px;">
      ${value}
      </div>
      `;

    if (key[0] === '>') {
      emojiArray.push(eachValue);
    }
  });
  return emojiArray;
};

const init = (target) => {
  showEmojiList(target);
  addEmojiButton(target);
};

export default {
  init,
};
