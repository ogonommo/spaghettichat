const play = (sound) => {
  if (sound === 'message') {
    document.getElementById('audio-message').play();
    return;
  }
  if (sound === 'intro') {
    document.getElementById('audio-intro').play();
    return;
  }
};

export default {
  play,
};
