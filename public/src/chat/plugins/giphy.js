/**
 * Giphy module
 */
import $ from 'jquery';
import template from '../view/templates';
import player from '../plugins/sounds';

// giphy API key
const GIPHY_API_KEY = 'Fzf72NPG7pUHuF8USghLGmhsRpkR7Sar';

// globals
let hasActiveCard = false;
let card;
let parent;
let notifier;
let info;
let sender = () => {};

// initialize the plugin
const init = (holder) => {
  parent = $(holder);
};

// return if there is an active card preview
const isActive = () => {
  return hasActiveCard;
};

// get giphy images from API
const fetchGiphyImages = async (keywords) => {
  // initialize the card
  try {
    const returned = await fetch(encodeURI(`https://api.giphy.com/v1/gifs/search?q=${keywords}&api_key=${GIPHY_API_KEY}&limit=20`));
    const result = await returned.json();
    return result.data;
  } catch (err) {
    return new Error(err);
  }
};

// initialize a card preview
const initCard = (keywords) => {
  // get the result images and create the card or notify with error
  fetchGiphyImages(keywords)
      .then((data) => createCard(data))
      .catch((err) => notifier(err));
};

// pulls the user info from ui.js
const onUserInfo = (getUserInfo) => {
  info = getUserInfo();
};

// create the card preview and hook button events
const createCard = (images) => {
  // do nothing if there is no result returned
  if (!images || !images.length) {
    return;
  }

  // there is an active selector card
  hasActiveCard = true;

  // get original and small size urls
  const originalGifs = images.map((image) => image.images.original.url);
  const smalllGifs = images.map((image) => image.images.fixed_width.url);

  // current image in the loop
  let current = 0;
  const compiledCard = template.compileGiphyCardPreview(smalllGifs[current]);
  parent.append(compiledCard);
  card = $('#giphy');

  // assign the listeners to the card
  // send the selected url, append it to the chat
  $('#sendgiphy').on('click', function() {
    card.remove();
    // run command is a text message
    sender(originalGifs[current]);
    updateChatPane(originalGifs[current]);
    // there is no longer an active card
    hasActiveCard = false;
  });

  // remove the card from view
  $('#cancelgiphy').on('click', function() {
    card.remove();
    // there is no longer an active card
    hasActiveCard = false;
  });

  // show the next giphy image in the sequence
  $('#shufflegiphy').on('click', function() {
    current++;
    if (current >= smalllGifs.length) {
      current = 0;
    }
    card.find('img').attr('src', smalllGifs[current]);
  });
};

// hook to the send function from ui.js
const onCardSubmit = (send) => {
  // pull the active template and send
  sender = send;
};

// hook to the toast notifier
const onFail = (notify) => {
  notifier = notify;
};

// dummy function, no use af of yet
const onStatusUpdate = (update) => {
  update();
};

// add the incoming card message to the chat
const addNewGiphyMessage = (data) => {
  // append the compiled template;
  parent.append(template.compileGiphyCard(data.username, data.chatcolor, data.date, data.message));
  player.play('message');
};

// add selected card from the preview to the chat
const updateChatPane = (image) => {
  // append the compiled template;
  parent.append(template.compileGiphyCard(info.username, info.chatcolor, new Date().toLocaleTimeString().slice(0, 5), image));
  player.play('message');
};

export default {
  init,
  isActive,
  initCard,
  onCardSubmit,
  onUserInfo,
  onFail,
  onStatusUpdate,
  addNewGiphyMessage,
};
