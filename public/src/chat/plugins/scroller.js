import $ from 'jQuery';

// globals
let scroller;
let bottom = true;

// initialize the scroller
const init = (element) => {
  scroller = $(element);

  scroller.scroll(onScroll);

  scroller.on('DOMSubtreeModified', update);
};

// hook to the scroll event
const onScroll = () => {
  // check if user has scrolled eslewhere
  const scrollHeight = scroller.prop('scrollHeight');
  const scrollPos = scroller.prop('scrollTop');
  const clientHeight = scroller.prop('clientHeight');
  if (Math.abs(scrollPos - (scrollHeight - clientHeight)) < 2) {
    bottom = true;
  } else {
    bottom = false;
  }
};

// update scroll position
const update = () => {
  // scroll to the bottom if the user hasn't scrolled elsewhere
  if (bottom) {
    scroller.scrollTop(scroller.prop('scrollHeight') -
      scroller.prop('clientHeight'));
  }
};

export default {
  init,
};
