import $ from 'jquery';
import template from '../view/templates';

// globals
let parent;

// initialize the userlist
const init = (holder) => {
  parent = $(holder);
};

// updates the list of the current users in chat
const update = (users) => {
  $('#userlist').remove();
  parent.append(template.compileUserList(users));
};

export default {
  init,
  update,
};
