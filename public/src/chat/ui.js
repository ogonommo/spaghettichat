import $ from 'jquery';
import M from 'materialize-css';
import giphy from './plugins/giphy';
import scroller from './plugins/scroller';
import userlist from './plugins/userlist';
import player from './plugins/sounds';
import autocomplete from './plugins/autocomplete/autocomplete';
import emoji from './plugins/emoji';
import parser from './parser/parser';
import template from './view/templates';
import chatinfo from './helpers/chatinfo';

// globals
const colors = {
  color1: 'black',
  color2: 'palevioletred',
  color3: 'yellowgreen',
  color4: 'slateblue',
  color5: 'crimson',
  color6: 'gold',
  color7: 'darkblue',
  color8: 'dimgray',
  color9: 'green',
  color10: 'orange',
};
let username;
let chatcolor = 'black';
let enabled = false;

// initialization logic here
const init = () => {
  // hook the color picker
  $('div.colorbox').on('click', function(e) {
    $('div.colorbox').removeClass('selected');
    const $this = $(e.target);
    $this.addClass('selected');
    chatcolor = colors[$this.attr('id')];
  });

  // initialize giphy
  giphy.init('#chatpane');
  giphy.onFail(notify);

  // initialize userlist
  userlist.init('#user-list');

  // initialize scroller
  scroller.init('#chatpane');

  // initialize autocomplete
  autocomplete.init('#messageinput');

  // initialize emoji
  emoji.init('#messageinputline');
};

// enable the chat pane
const enable = () => {
  enabled = true;
};

// disable the chat pane
const disable = () => {
  enabled = false;
};

// toast notifier
const notify = (message) => {
  M.toast({
    html: `${message}`,
  });
};

// trigger login event
const onLogin = (loginFunction) => {
  // throw if loginFunction is not a function
  if (typeof loginFunction !== 'function') {
    throw new Error(`onLogin: expected function, 
    got ${typeof loginFunction} instead`);
  }

  // login the user, hide the login form and show the chatpane
  $('#login').on('click', () => login(loginFunction));
};

// login function
const login = (loginFunction) => {
  // get the username
  const $input = $('#username');
  username = $input.val().trim();

  if (username && username.length < 13) {
    // there is an username in the input, update
    $('#login-page').hide();
    $('#master-chat').show();

    // update giphy with the user info
    giphy.onUserInfo(() => {
      return {
        chatcolor,
        username,
      };
    });

    // connect to the socket
    loginFunction({
      username,
      chatcolor,
    });

    // update the chat with welcome and app info
    $('#chatpane').append(`<div>
      ${template.intro(chatinfo.version, chatinfo.author)}
    </div`);

    // play intro sound
    player.play('intro');
  } else {
    // username is too long
    if (username.length > 12) {
      notify(`Username is too long.`);
      return;
    }
    // no username, notify the user to enter one
    notify(`Please enter username.`);
  }
};

// hook to the send funcionality
const onSendMessage = (emitmessage) => {
  // hook giphy to the sender
  giphy.onCardSubmit((gif) => {
    const message = {
      message: gif,
      date: new Date().toLocaleTimeString().slice(0, 5),
      type: 'giphy',
      username,
      chatcolor,
    };
    emitmessage(JSON.stringify(message));
  });

  // hook to the keyup event when the pressed key is ENTER
  const $input = $('#messageinput');
  $input.on('keyup', function(e) {
    if (!enabled) {
      return;
    }

    // check if pressed key is ENTER
    if (e.which === 13 && !autocomplete.isActive()) {
      const inputtext = $input.val().trim();

      // do nothing if there is no input
      if (!inputtext.length) {
        return;
      }

      // clear the chat if command is /clear
      if (inputtext.indexOf('/clear') === 0) {
        $('#chatpane').html('');
        $input.val('');
        return;
      }

      // run command is giphy
      if (inputtext.indexOf('/giphy ') === 0 && !giphy.isActive()) {
        // get the search terms
        const searchString = inputtext.slice(7);

        // initialize the card if there isn't an active one
        if (!giphy.isActive()) {
          giphy.initCard(searchString);
        }
        $input.val('');
        return;
      }

      // run command is a text message
      const _text = $input.val().trim();
      const parsedtext = parser.parse(parser.filter(parser.escapeHTMLEntities(_text)));
      const message = {
        message: parsedtext,
        date: new Date().toLocaleTimeString().slice(0, 5),
        type: 'text',
        username,
        chatcolor,
      };

      // push the message to logged users and empty the type box
      emitmessage(JSON.stringify(message));
      addMessage(message);
      $input.val('');
    }
  });
};

// update list of active users
const updateUsers = (users) => {
  userlist.update(users);
};

// add a text message in the chat and play sounds
const addMessage = (data) => {
  $('#chatpane').append(template.compileTextMessage(data.username, data.chatcolor, data.date, data.message));
  player.play('message');
};

// add a giphy message in the chat
const addGiphyMessage = giphy.addNewGiphyMessage;

export default {
  init,
  enable,
  disable,
  notify,
  onLogin,
  addMessage,
  addGiphyMessage,
  onSendMessage,
  updateUsers,
};
