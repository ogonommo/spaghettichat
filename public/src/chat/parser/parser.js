import cursewords from './cursewords';
import bbCodeParser from 'js-bbcode-parser';

// remove "<tag>" and "</tag>" tags from the input string
// and return the result;
const stripHTMLEntities = (input) => {
  const cleanText = input.replace(/<\/?[a-z][a-z0-9]*[^<>]*>/ig, '');
  return cleanText;
};

// escape "<" and ">" with the html entities
// replace "<" with "&lt;" and ">" with "&gt;"
const escapeHTMLEntities = (input) => {
  const fn = function(tag) {
    const charsToReplace = {
      '&': '&amp;',
      '<': '&lt;',
      '>': '&gt;',
      '"': '&#34;',
    };
    return charsToReplace[tag] || tag;
  };
  return input.replace(/[&<>"]/g, fn);
};

// use the cursewords array to filter out profanities
// replace profanities with "****"
const filter = (input) => {
  cursewords.forEach(function(word) {
    let stringOfStars = '';
    for (let i = 0; i < word.length; i++) {
      stringOfStars += '*';
    }
    let lowerCaseString = input.toLowerCase();
    while (lowerCaseString.includes(word)) {
      const indexOfWord = lowerCaseString.indexOf(word);
      input = input.split('');
      input.splice(indexOfWord, stringOfStars.length, stringOfStars);
      input = input.join('');

      lowerCaseString = lowerCaseString.split('');
      lowerCaseString.splice(indexOfWord, stringOfStars.length, stringOfStars);
      lowerCaseString = lowerCaseString.join('');
    }
  });
  return input;
};

// replace microcodes "[b]", etc. with key-value pairs
// from the map object in microcodes
const parse = (input) => {
  return bbCodeParser.parse(input);
};

export default {
  stripHTMLEntities,
  escapeHTMLEntities,
  filter,
  parse,
};
