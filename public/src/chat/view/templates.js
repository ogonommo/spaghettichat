/**
 * Chat message templates
 */


// giphy preview card template
const compileGiphyCardPreview = (imageLink) => {
  return `<div id="giphy">
  <p>This is a preview visible only to you.</p>
  <div class="giphyimage">
  <img src="${imageLink}" class="giphyimage" /><br />
  <div class="giphy-menu">
  <a class="waves-effect waves-light btn" id="sendgiphy">Send</a>
  <a class="waves-effect waves-light btn" id="shufflegiphy">Shuffle</a>
  <a class="waves-effect waves-light btn" id="cancelgiphy">Cancel</a>
    </div>
    </div>
    </div>`;
};

// giphy card template
const compileGiphyCard = (username, chatcolor, date, imageLink) => {
  return `<div class="messagebox">
  <div class="user">
    <span style="color: ${chatcolor};">
      <strong>${username}</strong>
    </span> <span class="date">${date}</span>
  </div>
  <div class="message"><img src="${imageLink}" class="giphyimage" /></div>
</div>`;
};

// default text message template
const compileTextMessage = (username, chatcolor, date, message) => {
  return `<div class="speech-bubble">
  <div class="user">
    <span style="color: ${chatcolor};">
      <strong>${username}</strong>
    </span> <span class="date">${date}</span>
  </div>
  <div class="message">${message}</div>
</div>`;
};

// active users list template
const compileUserList = (users) => {
  let list = ``;
  if (users.length) {
    users.forEach((user) => {
      list += `<li>
      <span id="color2" class="colorbox z-depth-1" style="background-color: ${user.chatcolor};"></span>
      <span style="color: ${user.chatcolor};">
      <strong>${user.username}</strong>
      </span>
      </li>`;
    });
  }
  return `<ul id="userlist">${list}</ul>`;
};

// welcome to chat intro template
const intro = (version, author) => {
  return `Welcome to -- Spaghetti Chat --<br/>
  Version: ${version}<br/>
  Author: ${author}<br/>
  --------------------<br/>
  <br/>`;
};

export default {
  compileGiphyCard,
  compileGiphyCardPreview,
  compileTextMessage,
  compileUserList,
  intro,
};
