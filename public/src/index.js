// Chat functions
import io from 'socket.io-client';
import ui from './chat/ui';

// global variables
const socket = io();
let loggedUsers = [];

const status = {
  connected: false,
};

// initialize the ui
ui.init();

// hook the login
ui.onLogin((data) => {
  socket.emit('new user', data);
});

// update the users list when a new one joins
socket.on('user joined', (data) => {
  ui.updateUsers(data.users);
});

// hook the message send
ui.onSendMessage((data) => {
  socket.emit('new message', data);
});

socket.on('login', (data) => {
  // enable the chat
  ui.enable();

  // update the logged users
  ui.updateUsers(data.users);
  loggedUsers = data.users;

  // show last 20 messages
  const messages = data.messages;
  if (messages.length) {
    messages.forEach((message) => {
      const deserialized = JSON.parse(message);
      if (deserialized.type === 'text') {
        ui.addMessage(deserialized);
      }
      if (deserialized.type === 'giphy') {
        ui.addGiphyMessage(deserialized);
      }
    });
  }
});

// update the active users list when someone leaves
socket.on('disconnected', (data) => {
  ui.updateUsers(data.users);
});

// update the chat on new message received
socket.on('new message', (data) => {
  const message = JSON.parse(data);
  if (message.type === 'text') {
    ui.addMessage(message);
  }
  if (message.type === 'giphy') {
    ui.addGiphyMessage(message);
  }
});
