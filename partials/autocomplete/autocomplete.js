import { fragments } from './fragments';
import $ from 'jquery';

// $ exist as a standard <script> import in the html file
// this is a temporary fix

// implement the autocomplete feature as you see fit

const commandsList = [];
fragments.forEach((child) => {
  commandsList.push(child);
});
console.log(commandsList);

// ///////////////////////////

let autocompleteContainer = '';
const resultsArray = [];
let resultCursor = 0;
const autocompleteFocus = ( target ) => {
  $(target).focus();
};

const isEnabled = ( ) => {
  if ($('.autocomplete-container').length) {
    return true;
  }

  return false;
};

const eachResultClick = ( target ) => {
  $(document.body).on('click', 'div.result', function() {
    // console.log($( '.result' ).length);
    $( '.result' ).removeClass('highlighted');
    $(this).addClass('highlighted');
    const commandText = $(this).children()[0].innerHTML.trim();
    $( target ).val( commandText );
    $(target).focus();
  });
};

const eachButtonPress = ( target ) => {
  $(document.body).on('keyup', ('#messageinput'), function(event) {
    $( '.autocomplete-container' ).remove();
    // console.log($( '#autocomplete-container' ));
    let matchingCommands = [];

    // console.log($(target).val().length );
    if ($(target).val().length > 0) {
      matchingCommands = getMatches( $( target ).val());
    }

    if (matchingCommands.length) {
      autocompleteDisplay( target, matchingCommands );

      // ///////////////////////////
      const resultsLength = $('.autocomplete-container').children().length;
      let currentResult = $('.autocomplete-container').children()[resultCursor];
      $( currentResult ).addClass('highlighted');

      // This stll submits before autocompleting and does not track event.stopImmediatePropagation(); nor event.preventDefault();
      switch ( event.keyCode ) {
        // enter key
        case 13:
          if ( $('.autocomplete-container').length) {
            const newInputValue = $( currentResult).children()[0].innerHTML.trim();
            $( target ).val( newInputValue );
            $('.autocomplete-container').remove();
            resultCursor = 0;
          }
          break;
        // arrow up
        case 38:
          if (resultCursor > 0) {
            resultCursor -= 1;
            currentResult = $('.autocomplete-container').children()[resultCursor];

            $( '.result' ).removeClass('highlighted');
            $( currentResult ).addClass('highlighted');
            // console.log($(currentResult));
          }

          break;
        // arrow down
        case 40:
          if (resultCursor < resultsLength - 1) {
            resultCursor += 1;
            currentResult = $('.autocomplete-container').children()[resultCursor];

            $( '.result' ).removeClass('highlighted');
            $( currentResult ).addClass('highlighted');
            // console.log($(currentResult));
          }
          break;
        case 8:
          resultCursor = 0;
          break;

        default:
          break;
      }

      // console.log(autocompleteIsEnabled());
    }
  });
};


const getMatches = ( targetInput ) => {
  const matchedCommandsArray = [];
  const input = targetInput.toLowerCase();
  // console.log('input: ', input);
  // exporting commandsList from fragments.js
  commandsList.forEach((item) => {
    if (item.command.toLowerCase().includes(input)) {
      matchedCommandsArray.push(item);
      // console.log( item.command );
    }
  });
  return matchedCommandsArray;
};


const autocompleteDisplay = ( target, commands ) => {
  let resultsContainer = '';

  commands.forEach((item) =>{
    // console.log('item:', item);
    const result = `
    <div class="result">
      <div class="result-name"> ${item.command}  </div>
      <div class="result-desc"> ${item.description} </div>
     </div>`;
    // console.log( result );

    // console.log(result);
    resultsContainer += result;
    resultsArray.push(result);

    autocompleteContainer = `
  <div class="autocomplete-container">
  ${resultsContainer}
  </div>`;

    // console.log('autocomplete code: ', autocompleteContainer);
    $( '.autocomplete-container' ).remove();
    $( autocompleteContainer ).insertBefore($( target ));
  });
  // console.log( $( '.autocomplete-container' ).length);
};


// //////////////////////////////

const init = (target) => {
  autocompleteFocus( target );
  eachButtonPress( target );
  eachResultClick( target );
};

export default {
  init,
  isEnabled,
};


