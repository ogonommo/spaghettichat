export const fragments = [{
  command: '/giphy',
  description: `/giphy text-for-gif-search<br/>
  insert a gif from Giphy
  use "Shuffle" to cicle through available gifs
  use "Send" to send the selected gif
  use "Cancel" to cancel the command`,
},
{
  command: '/clear',
  description: `/clear
  clears the chat window`,
},
{
  command: '/notify',
  description: `/notify text
  elevates the "text" message to grab the attention
  of everyone in the channel`,
},
];
