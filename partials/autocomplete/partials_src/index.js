// Chat functions
import io from 'socket.io-client';
import ui from './chat/ui';
import autocomplete from './autocomplete/autocomplete';

// global variables
const socket = io();
let loggedUsers = [];

const status = {
  connected: false,
};

// initialize the ui
ui.init();

// hook the login
ui.onLogin((data) => {
  socket.emit('new user', data);
  autocomplete.init( '#messageinput' );
});

// hook the message send
ui.onSendMessage((data) => {
  socket.emit('new message', data);
});

socket.on('login', (data) => {
  ui.enable();
  loggedUsers = data.users;
  const messages = data.messages;
  if (messages.length) {
    messages.forEach((message) => {
      const deserialized = JSON.parse(message);
      if (deserialized.type === 'text') {
        ui.addMessage(deserialized);
      }
      if (deserialized.type === 'giphy') {
        ui.addGiphyMessage(deserialized);
      }
    });
  }
});

socket.on('new message', (data) => {
  const message = JSON.parse(data);
  if (message.type === 'text') {
    ui.addMessage(message);
  }
  if (message.type === 'giphy') {
    ui.addGiphyMessage(message);
  }
});
