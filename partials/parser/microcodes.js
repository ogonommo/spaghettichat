export default new Map([
  ['[b]', '<strong>'],
  ['[/b]', '</strong>'],
  ['[i]', '<em>'],
  ['[/i]', '</em>'],
  ['[u]', '<u>'],
  ['[/u]', '</u>'],
  ['>spaghetti', '🍝'],
  ['>joy', '😂'],
  ['>smile', '😀'],
  ['>grinning-sweat', '😅'],
  ['>tasty', '😋'],
  ['>love', '😍'],
  ['>cool', '😎'],
  ['>thinking', '🤔'],
  ['>eye-roll', '🙄'],
  ['>confounded', '😣'],
  ['>angry', '😤'],
  ['>worried', '😟'],
  ['>cry', '😭'],
  ['>sad', '😢'],
  ['>grimace', '😬'],
  ['>flushed', '😳'],
  ['>pouting', '😡'],
  ['>halo', '😇'],




]);
