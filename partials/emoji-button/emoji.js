import emojiMap from '../parser/microcodes';
import $ from 'jquery';


const addEmojiButton = ( target ) => {
  const emoButton =`
  <div class="waves-effect btn-flat white" id="emoji-button">
  <i class="material-icons medium" id="emoji-button-icon">insert_emoticon</i>
  </div>`;

  const emojiTable = `
    <div id="emoji-table" class="white scale-transition scale-out">
    </div> `;

  $(target).append(emoButton);
  $(target).append(emojiTable);
};

const showEmojiList = ( target ) => {
  $( target ).on('click', 'div#emoji-button', function() {
    toggleScale($( '#emoji-table' ));
    // console.log('Hey!');
  });
};

const appendEmojiToInput = ( target ) => {
  // console.log(target);
  // console.log($( 'div.emoji' ));
  $( 'div.emoji' ).on('click', function() {
    const emojiText = $(this).text().trim();
    $( 'input#messageinput').val($( 'input#messageinput').val() + emojiText );
    $( 'input#messageinput').focus();
  });
};


// //////// Insider Functions Start ////////

const toggleScale = ( target ) => {
  if ( $( target ).hasClass('scale-out')) {
    $( target ).removeClass('scale-out').addClass('scale-in');
    // Appending emoji table here so it's only loaded on scale-in
    $('#emoji-table').append(loadEmojiTable());
    appendEmojiToInput( target );
  } else {
    $( target ).removeClass('scale-in').addClass('scale-out');
    $('#emoji-table').empty();
  }
};


const loadEmojiTable = () => {
  const emojiArray = [];
  emojiMap.forEach((value, key) => {
    const eachValue = `
      <div class="col s2 emoji" style="padding: 5px;">
      ${value}
      </div>
      `;

    if (key[0] === '>') {
      emojiArray.push(eachValue);
    }
  });
  return emojiArray;
};

// //////// Insider Functions End ////////

const init = (target) => {
  showEmojiList( target );
  addEmojiButton( target );
};

export default {
  init,
};

